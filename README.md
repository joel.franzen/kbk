Current status: __ALIVE__ (Kind of...)

To Get things running:

Download the repo

`https://gitlab.com/bubthegreat/kbk.git`

Install Docker from [here](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

Run the docker compose command:
`docker-compose up`

Now you should be able to connect to port 8989 of your host it's running on.

NOTE: Still have some bugs to work out with the database connections...Not sure what's up there.
